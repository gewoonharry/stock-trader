export default [
    { id: 1, name: 'BMW', price: 110 },
    { id: 2, name: 'Google', price: 200 },
    { id: 3, name: 'Apple', price: 250 },
    { id: 4, name: 'Twitter', price: 8 },
    { id: 5, name: 'Facebook', price: 35 },
    { id: 6, name: 'Amazon', price: 230 },
    { id: 7, name: 'Tesla', price: 300 },
    { id: 8, name: 'Mercedes', price: 120 }
];
